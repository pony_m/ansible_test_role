# Role ansible-test-role

## Installation

Add the following to your repo's `requirements.yml`:

    - src: git@gitlab.com:pony_m/ansible_test_role.git
      scm: git
      version: main

## Testing

    python3 -m venv .venv && source venv/bin/activate

Install requirements

    pip install -r requirements.txt

Run all distros molecule tests like in CI

    ./scripts/molecule.sh
    # ./scripts/molecule.sh converge

Run the test in full for the default OS cookiecutter.supported_os.split(',')[0] (`--destroy never` to debug)

    molecule test
    molecule converge

## Debugging

Debug the containers

    molecule login

Destroy the environment

    molecule destroy
