#!/usr/bin/env bash

set -eu

distros=("ubuntu2204" "ubuntu2004" )

if [ $# -eq 0 ]; then
    args="test"
else
    args="$@"
fi

echo "Running: molecule $args"

for item in "${distros[@]}"; do
    echo "---- Distro: $item"
    echo ""
    molecule $args
done
